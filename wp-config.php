<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'socaldynasty' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'advanced' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|vs1Y{Gi2xno1=)JvOgNGI8&4/CWoZCF!F@v#QW(<}XvsS~]nBAP@ER%^fo<D]xg' );
define( 'SECURE_AUTH_KEY',  'VkjP0PlHlv15(^VUw4_Y~4C>L.MaG0fiNt.fYtbU&GRK1uO>uU99X3ax[^^ux&aB' );
define( 'LOGGED_IN_KEY',    'DJ@1?>Du;ynV e-|pw*w1>[#8T|}FG&$Ku._2g/],V!Ih gPl08GU2+kSG0U:T5-' );
define( 'NONCE_KEY',        '5$(2Q`)J;+h^HUz14jQ97UIJ?p&8h_/~TsBWon9{vnX)G:r!P5r0*uas-4sGi[hl' );
define( 'AUTH_SALT',        'Ww+P%d4vhXH.M;1<`>Gr5%K0-`CC[~3IU IBfUa/IjD3U4S;8MZ_@i ufh4USLpM' );
define( 'SECURE_AUTH_SALT', '|A>QyE~uoCwtC6WG-Off+Ku>bCLgdKLv&kJ[R1sBptL:D3`JJGCw<3tFHIUmV1jL' );
define( 'LOGGED_IN_SALT',   '.8F4I]5Z5v2o~|oAgthD,Ohu~ IGy*9,vn!9>Mg/h!9/(%5Yo[ckG2lnb9<[KB>@' );
define( 'NONCE_SALT',       '>&AFJ6*U.Xpy?G;c9u<W~8oEH@^^!4GyWhR(fF4`U^9eM7(Q[P]S/ X 1W5$+N%*' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
